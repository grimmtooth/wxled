#! /c/Python/python
#----------------------------------------------------------------------
# Name:         wxLED
# Purpose:      A class that simulates an LED indicator
#
# Author:       Jeff Grimmett (grimmtooth@softhome.net)
#
# Copyright:    (c) 2018 by GrimmLabs
# Licence:      wxWindows license
#----------------------------------------------------------------------

import	wx

import version

__version__ = version.version


#----------------------------------------------------------------------
LED_COLOR_RED   = LED_COLOUR_RED = 0
LED_COLOR_GREEN = LED_COLOUR_GREEN = 1 << 1
LED_COLOR_AMBER = LED_COLOUR_AMBER = 1 << 2
LED_COLOR_CUSTOM = LED_COLOUR_CUSTOM = 1 << 3

LED_SHAPE_ROUND = 0
LED_SHAPE_RECTANGLE = 1 << 1
LED_SHAPE_TRIANGLE = 1 << 2
LED_SHAPE_SQUARE = 1 << 3

LED_POINT_UP = 0
LED_POINT_DOWN = 1 << 1
LED_POINT_LEFT = 1 << 2
LED_POINT_RIGHT = 1 << 3
#----------------------------------------------------------------------

class LEDCtrl(wx.Window):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=wx.Size(25, 25), colorStyle = LED_COLOR_RED,
                 shapeStyle = LED_SHAPE_ROUND, orientation = 0, style = wx.SUNKEN_BORDER , OnColor = None,
                 OffColor = None):

        print('LEDCtrl.__init__: start.')
        
        self.state = False
        
        if ID == -1:
            ID = wx.NewId()

        self.colorStyle = colorStyle
        self.shapeStyle = shapeStyle
        self.orientation = orientation
        self.OnColor = OnColor
        self.OffColor = OffColor

        # define color
        if self.colorStyle == LED_COLOR_RED:
            self.Lit = wx.RED
            self.Dark = wx.Colour(145, 0, 0)
        
        wx.Window.__init__(self, parent, ID, pos=pos, size=size, style=style)

        print('LEDCtrl.__init__: wx.Window.__init__ complete.')

        self.Bind(wx.EVT_PAINT, self.OnPaint)
        
    def OnPaint(self, evt):
        dc = wx.PaintDC(self)
        
        w, h = self.GetClientSize()
        print(f"LEDCtrl.OnPaint: size is {w}, {h}")

        if self.state:
            dc.SetBrush(wx.Brush(self.Lit))
        else:
            dc.SetBrush(wx.Brush(self.Dark))

        # round LED
        if self.shapeStyle == LED_SHAPE_ROUND:
            radius = min(w, h) / 2
            center = (w / 2, h / 2)
            dc.DrawCircle(center, radius)

        # wx.Window.OnPaint(self, evt)
        evt.Skip()

    def SetState(self, state):
        print(f'LEDCtrl.SetState: setting state to {state}')

        if state == True:
            self.state = True
        else:
            self.state = False
    
        self.Refresh()

    def ToggleState(self):
        print('LEDCtrl.ToggleState: Toggling state')
        if self.state == True:
            self.SetState(False)
        else:
            self.SetState(True)

#----------------------------------------------------------------------


class _TestPanel(wx.Panel):
    def __init__(self, parent):
        
        self.parent = parent
        self.gadlist = []
        
        wx.Panel.__init__(self, parent, -1)
        sizer = wx.GridBagSizer(5, 5)

        t = (   "This demo shows the various kinds of LED controls supported\n"
                "by the LEDCtrl class. "
            )
        
        sizer.Add(
            wx.StaticText(self, -1, t), 
            (1, 1), (1, 5), wx.ALIGN_CENTER | wx.ALL, 5
            )

        c = LEDCtrl(self, -1)
        b = wx.ToggleButton(self, -1, "Click")
        t = wx.StaticText(self, -1, "Default size, color and shape.")
        
        sizer.Add(c, (2, 1), (1, 1), wx.ALIGN_RIGHT | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 1)
        sizer.Add(b, (2, 2), (1, 2), wx.ALIGN_CENTER | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 1)
        sizer.Add(t, (2, 4), (1, 3), wx.ALIGN_LEFT | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 1)
        self.gadlist.append((b, c))
        self.Bind(wx.EVT_TOGGLEBUTTON, self.onToggle, b)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def onToggle(self, evt):
        for button, led in self.gadlist:
            if button == evt.GetEventObject():
                led.SetState(button.GetValue())
                break


class _TestFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, "Test Frame")
        self.panel = _TestPanel(self)
        self.Show(True)


class __TestApp(wx.App):

    def OnInit(self):
        self.SetAppName("LED Test")
        self.SetVendorName("Industrial Strength Software")

        self.frame = _TestFrame()
        self.SetTopWindow(self.frame)
        self.SetExitOnFrameDelete(True)
        return True

    def	OnCloseApp(self, event):
        while self.Pending():
            self.Dispatch()


# De main, de main!
if __name__ == "__main__":
    import  sys
    
    app = __TestApp(False)
    app.MainLoop()
    sys.exit(0)

